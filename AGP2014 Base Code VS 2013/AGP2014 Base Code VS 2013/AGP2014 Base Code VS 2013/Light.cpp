#include "Light.h"
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>
using namespace glm;

Light::Light() : Light(vec4(0.0f), vec4(0.0f), vec4(0.0f), vec4(0.0f), vec3(0.0f))
{

}

Light::Light(glm::vec4 ambient, glm::vec4 diffuse, glm::vec4 specular, glm::vec4 position, glm::vec3 attenuation)
{
	this->position = position;
	this->ambient = ambient;
	this->diffuse = diffuse;
	this->specular = specular;
	this->attenuation = attenuation;
}

Light::~Light()
{
}
