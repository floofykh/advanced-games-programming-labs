#pragma once
#include "Shader.h"
#include "Texture.h"

class Model
{
protected:
	Shader *shader = nullptr;
	Material *material = nullptr;
	Texture *texture = nullptr;
	GLuint indexCount, vertexCount, id;
	glm::vec3 position, scale, rotation;
public:
	Model();
	void setShader(Shader *shader);
	void setMaterial(Material *material);
	void setTexture(Texture *texture);
	bool load(std::string file);
	virtual void draw(glm::mat4 viewMat, glm::mat4 projMat);
	virtual void drawRawData();
	void setPosition(glm::vec3 position) { this->position = position; }
	void setScale(glm::vec3 scale) { this->scale = scale; }
	void setRotate(glm::vec3 rotation) { this->rotation = rotation; }
	void rotate(glm::vec3 rotation){ this->rotation += rotation; }
	void getIDAndIndexCount(GLuint &id, GLuint &indexCount){ id = this->id; indexCount = this->indexCount; }
	~Model();
};

