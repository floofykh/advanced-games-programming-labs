#include "Model.h"
#include "rt3dObjLoader.h"
#include <vector>
#include <iostream>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include "md2model.h"

using namespace std;
using namespace glm;

Model::Model() : position(1.0f), scale(1.0f), rotation(1.0f)
{
	
}

void Model::setShader(Shader *shader)
{
	this->shader = shader;
}

void Model::setTexture(Texture *texture)
{
	this->texture = texture;
}
void Model::setMaterial(Material *material)
{
	this->material = material;
}

bool Model::load(std::string file)
{
		vector<GLfloat> cubeVerts;
		vector<GLfloat> cubeNorms;
		vector<GLfloat> cubeTex_coords;
		vector<GLuint> cubeIndices;
		rt3d::loadObj(file.c_str(), cubeVerts, cubeNorms, cubeTex_coords, cubeIndices);
		indexCount = cubeIndices.size();
		vertexCount = cubeVerts.size();

		// generate and set up a VAO for the mesh
		glGenVertexArrays(1, &id);
		glBindVertexArray(id);


		if (cubeVerts.size() == 0) {
			// cant create a mesh without vertices... oops
			cout << "Model from " << file << " not loaded correctly." << endl;
			return false;
		}

		// generate and set up the VBOs for the data
		GLuint VBO;
		glGenBuffers(1, &VBO);

		// VBO for vertex data
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, vertexCount*sizeof(GLfloat), cubeVerts.data(), GL_STATIC_DRAW);
		glVertexAttribPointer(Shader::POSITION, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(Shader::POSITION);

		// VBO for normal data
		if (cubeNorms.size() != 0) {
			glGenBuffers(1, &VBO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, vertexCount*sizeof(GLfloat), cubeNorms.data(), GL_STATIC_DRAW);
			glVertexAttribPointer(Shader::NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(Shader::NORMAL);
		}

		// VBO for tex-coord data
		if (cubeTex_coords.size() != 0) {
			glGenBuffers(1, &VBO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, vertexCount*sizeof(GLfloat), cubeTex_coords.data(), GL_STATIC_DRAW);
			glVertexAttribPointer(Shader::TEXTURE, 2, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(Shader::TEXTURE);
		}

		if (cubeIndices.size() != 0) {
			glGenBuffers(1, &VBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount * sizeof(GLuint), cubeIndices.data(), GL_STATIC_DRAW);
		}
		// unbind vertex array
		glBindVertexArray(0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		return true;
}

void Model::draw(glm::mat4 viewMat, glm::mat4 projMat)
{
	if (shader != nullptr)
	{
		shader->bindProgram();
		if (material != nullptr)
			shader->setMaterial(material);
		if (texture != nullptr)
			texture->bind();
		
		mat4 modelMat(1.0f);
		modelMat = translate(modelMat, position);
		modelMat = glm::scale(modelMat, scale);
		modelMat = glm::rotate(modelMat, rotation.x, vec3(1.0f, 0.0f, 0.0f));
		modelMat = glm::rotate(modelMat, rotation.y, vec3(0.0f, 1.0f, 0.0f));
		modelMat = glm::rotate(modelMat, rotation.z, vec3(0.0f, 0.0f, 1.0f));

		mat4 modelViewMat = viewMat * modelMat;

		shader->setUniformMat4("modelMatrix", value_ptr(modelMat));
		shader->setUniformMat4("modelview", value_ptr(modelViewMat));
		shader->setUniformMat4("projection", value_ptr(projMat));

		drawRawData();
		Texture::unbind();
	}
}

void Model::drawRawData()
{
	glBindVertexArray(id);
	if (indexCount != 0)
	{
		glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
	}
	else
	{
		glDrawArrays(GL_TRIANGLES, 0, vertexCount);
	}
	glBindVertexArray(0);
}


Model::~Model()
{
}
