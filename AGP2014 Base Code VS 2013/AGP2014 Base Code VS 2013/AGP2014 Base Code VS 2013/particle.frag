#version 330

in vec4 ex_Colour;
out vec4 out_Colour;

uniform sampler2D textureUnit0;

void main()
{
	out_Colour = ex_Colour * texture(textureUnit0, gl_PointCoord);
}