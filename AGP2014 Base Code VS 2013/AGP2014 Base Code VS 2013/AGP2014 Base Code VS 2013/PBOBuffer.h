#pragma once
#include <GL\glew.h>
#include "Shader.h"
#include <glm\glm.hpp>
#include "Model.h"
class PBOBuffer
{
private:
	GLuint bufferIndex, texIndex, texWidth, texHeight;
	Shader *shader;
	Model *mesh;
public:
	glm::vec3 position, rotation, scale;

	PBOBuffer(GLuint screenWidth, GLuint screenHeight);
	void init();
	void draw(glm::mat4 viewMat, glm::mat4 projMat);
	void setDimensions(GLuint screenWidth, GLuint screenHeight);
	void setShader(Shader *shader) { this->shader = shader; }
	void setMesh(Model *mesh) { this->mesh = mesh; }
	~PBOBuffer();
};

