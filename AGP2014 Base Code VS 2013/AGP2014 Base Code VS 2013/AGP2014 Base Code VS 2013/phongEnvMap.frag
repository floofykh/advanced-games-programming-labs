#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec3 attenuation;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

uniform lightStruct light;
uniform vec4 lightPosition;
uniform materialStruct material;
uniform samplerCube cubeMap; 
uniform sampler2D texMap;

in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L;
in vec2 ex_TexCoord;
in float ex_distance;
in vec3 ex_WorldNorm;
in vec3 ex_WorldView;
out vec4 out_Color;
 
void main(void) {
	float attenuation = 1 / (light.attenuation.x + light.attenuation.y*ex_distance + light.attenuation.z*ex_distance*ex_distance);

	// Ambient intensity
	vec4 ambientI = light.ambient * material.ambient;

	// Diffuse intensity
	vec4 diffuseI = light.diffuse * material.diffuse;
	diffuseI = diffuseI * max(dot(normalize(ex_N),normalize(ex_L)),0) * attenuation;

	// Specular intensity
	// Calculate R - reflection of light
	vec3 R = normalize(reflect(normalize(-ex_L),normalize(ex_N)));

	vec4 specularI = light.specular * material.specular;
	specularI = specularI * pow(max(dot(R,ex_V),0), material.shininess) * attenuation;

	// Fragment colour
	vec4 litColor = (ambientI + diffuseI + specularI);
	
    vec3 reflectTexCoord = reflect(-ex_WorldView, normalize(ex_WorldNorm));
	out_Color = texture(cubeMap, reflectTexCoord) * texture(texMap, ex_TexCoord) * litColor;
	//out_Color = vec4(reflectTexCoord, 1.0);
}