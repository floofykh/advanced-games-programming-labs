#include "Shader.h"
#include <fstream>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>

using namespace std;
using namespace glm;

Shader::Shader(string vertex, string fragment, string name)
{
	this->name = name;
	vertexID = glCreateShader(GL_VERTEX_SHADER);
	fragmentID = glCreateShader(GL_FRAGMENT_SHADER);

	const char * fragmentSource, *vertexSource;
	int vertexSourceLength = 0, fragmentSourceLength = 0;

	fragmentSource = loadFile(fragment, fragmentSourceLength);
	vertexSource = loadFile(vertex, vertexSourceLength);

	glShaderSource(vertexID, 1, &vertexSource, &vertexSourceLength);
	glShaderSource(fragmentID, 1, &fragmentSource, &fragmentSourceLength);

	if (compile())
	{
		programID = glCreateProgram();

		glAttachShader(programID, vertexID);
		glAttachShader(programID, fragmentID);

		/*glBindAttribLocation(programID, ShaderAttributes::POSITION, "in_Position");
		glBindAttribLocation(programID, ShaderAttributes::COLOUR, "in_Color");
		glBindAttribLocation(programID, ShaderAttributes::NORMAL, "in_Normal");
		glBindAttribLocation(programID, ShaderAttributes::TEXTURE, "in_TexCoord");*/

		glLinkProgram(programID);
		glUseProgram(programID);


		GLuint uniformIndex = glGetUniformLocation(programID, "cubeMap");
		glUniform1i(uniformIndex, CUBEMAP);
		uniformIndex = glGetUniformLocation(programID, "texMap");
		glUniform1i(uniformIndex, TEXTUREMAP);
	}

	delete[] fragmentSource;
	delete[] vertexSource;
}

bool Shader::compile()
{
	GLint compiled;
	bool successful = true;

	glCompileShader(vertexID);
	glGetShaderiv(vertexID, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		cout << name << " vertex shader not compiled" << endl;
		printError(vertexID);
		successful = false;
	}

	glCompileShader(fragmentID);
	glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		cout << name << " fragment shader not compiled" << endl;
		printError(fragmentID);
		successful = false;
	}

	return successful;
}

void Shader::bindProgram()
{
	glUseProgram(programID);
	if (light != nullptr)
	{
		int uniformIndex = glGetUniformLocation(programID, "light.ambient");
		glUniform4fv(uniformIndex, 1, value_ptr(light->ambient));
		uniformIndex = glGetUniformLocation(programID, "light.diffuse");
		glUniform4fv(uniformIndex, 1, value_ptr(light->diffuse));
		uniformIndex = glGetUniformLocation(programID, "light.specular");
		glUniform4fv(uniformIndex, 1, value_ptr(light->specular));
		uniformIndex = glGetUniformLocation(programID, "light.attenuation");
		glUniform3fv(uniformIndex, 1, value_ptr(light->attenuation));
		uniformIndex = glGetUniformLocation(programID, "lightPosition");
		glUniform4fv(uniformIndex, 1, value_ptr(light->position));
	}
}

void Shader::setMaterial(Material *material)
{
	glUseProgram(programID);
	int uniformIndex = glGetUniformLocation(programID, "material.ambient");
	glUniform4fv(uniformIndex, 1, value_ptr(material->ambient));
	uniformIndex = glGetUniformLocation(programID, "material.diffuse");
	glUniform4fv(uniformIndex, 1, value_ptr(material->diffuse));
	uniformIndex = glGetUniformLocation(programID, "material.specular");
	glUniform4fv(uniformIndex, 1, value_ptr(material->specular));
	uniformIndex = glGetUniformLocation(programID, "material.shininess");
	glUniform1f(uniformIndex, material->shininess);
}

void Shader::setUniformMat4(std::string name, GLfloat *data)
{
	glUseProgram(programID);
	int uniformIndex = glGetUniformLocation(programID, name.c_str());
	glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, data);
}

void Shader::setUniformVec3(std::string name, GLfloat *data)
{
	glUseProgram(programID);
	int uniformIndex = glGetUniformLocation(programID, name.c_str());
	glUniform3fv(uniformIndex, 1, data);
}

void Shader::setUniform1i(std::string name, GLuint value)
{
	glUseProgram(programID);
	int uniformIndex = glGetUniformLocation(programID, name.c_str());
	glUniform1i(uniformIndex, value);
}

char* Shader::loadFile(string file, int &fileSize)
{
	int size;
	char * memblock;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	ifstream stream(file, ios::in | ios::binary | ios::ate);
	if (stream.is_open()) 
	{
		size = (int)stream.tellg(); // get location of file pointer i.e. file size
		memblock = new char[size];
		stream.seekg(0, ios::beg);
		stream.read(memblock, size);
		stream.close();
		cout << "Shader " << file << " loaded..." << endl;
	}
	else {
		cout << "Unable to open shader " << file << endl;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
		return nullptr;
	}

	fileSize = size;
	return memblock;
}
void Shader::printError(const GLint id)
{
	int maxLength = 0;
	int logLength = 0;
	GLchar *logMessage;

	// Find out how long the error message is
	if (!glIsShader(id))
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &maxLength);
	else
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);

	if (maxLength > 0) { // If message has some contents
		logMessage = new GLchar[maxLength];
		if (!glIsShader(id))
			glGetProgramInfoLog(id, maxLength, &logLength, logMessage);
		else
			glGetShaderInfoLog(id, maxLength, &logLength, logMessage);
		cout << "Shader Info Log:" << endl << logMessage << endl;
		delete[] logMessage;
	}
	// should additionally check for OpenGL errors here
}


Shader::~Shader()
{
}
