#pragma once
#include <glm\glm.hpp>
class Light
{
public:
	glm::vec4 position, ambient, diffuse, specular;
	glm::vec3 attenuation;
	Light();
	Light(glm::vec4 ambient, glm::vec4 diffuse, glm::vec4 specular, glm::vec4 position, glm::vec3 attenuation);
	~Light();
};

