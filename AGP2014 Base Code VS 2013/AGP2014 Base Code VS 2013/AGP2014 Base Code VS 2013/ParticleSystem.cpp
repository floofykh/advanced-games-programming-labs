#include "ParticleSystem.h"
#include <random>
#include <chrono>
#include <glm\gtc\type_ptr.hpp>

GLfloat acceleration[] = { 0.0f, -0.0001f, 0.0f };

ParticleSystem::ParticleSystem(const unsigned int num, glm::vec3 position) : position(position)
{
	numParticles = num;
	positions = new GLfloat[num * 3];
	colours = new GLfloat[num * 3];
	velocities = new GLfloat[num * 3];

	unsigned seed = (unsigned int)std::chrono::system_clock::now().time_since_epoch().count();

	//Create a default random number generator and seed it with current time
	std::default_random_engine generator(seed);
	//Create two distributions for the generator, one for the colour (float between 0 and 1), and one for the velocity(flaot between -0.1 and 0.1).
	std::uniform_real_distribution<float> colourDistribution(0.0, 1.0);
	std::uniform_real_distribution<float> velocityDistribution(-0.05, 0.05);


	for (unsigned int i = 0; i < numParticles * 3; i++)
	{

		if (i % 3 == 0)
			positions[i] = position.x;
		else if (i % 3 == 1)
			positions[i] = position.y;
		else
			positions[i] = position.z;

		colours[i] = colourDistribution(generator);
		velocities[i] = velocityDistribution(generator);
	}

	glGenVertexArrays(1, &vao);
	glGenBuffers(2, vbo);

	//Position data
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, numParticles * 3 * sizeof(GLfloat), positions, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	//Colour data
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, numParticles * 3 * sizeof(GLfloat), colours, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);
}

void ParticleSystem::update()
{
	for (unsigned int i = 0; i < numParticles*3; i++)
	{
		positions[i] += velocities[i];

		if (i % 3 == 0)
			velocities[i] += acceleration[0];
		else if (i % 3 == 1)
			velocities[i] += acceleration[1];
		else
			velocities[i] += acceleration[2];
	}
}

void ParticleSystem::draw(glm::mat4 viewMat, glm::mat4 projection)
{
	if (shader != nullptr && numParticles != 0)
	{
		if (texture != nullptr)
		{
			texture->bind();
		}
		glDepthMask(0);
		glEnable(GL_BLEND);
		glPointSize(10);
		glm::mat4 mvpMat = projection * viewMat;
		shader->bindProgram();
		shader->setUniformMat4("mvpMat", glm::value_ptr(mvpMat));

		glBindVertexArray(vao);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
		glBufferData(GL_ARRAY_BUFFER, numParticles * 3 * sizeof(GLfloat), positions, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glDrawArrays(GL_POINTS, 0, numParticles);
		glBindVertexArray(0);
		glDisable(GL_BLEND);
		glDepthMask(1);

		Texture::unbind();
	}
}


ParticleSystem::~ParticleSystem()
{
	if (positions != nullptr)
		delete[] positions;
	if (colours != nullptr)
		delete[] colours;
	if (velocities != nullptr)
		delete[] velocities;
}
