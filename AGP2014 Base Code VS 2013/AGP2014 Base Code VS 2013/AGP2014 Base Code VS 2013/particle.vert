#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 colour;
out vec4 ex_Colour;

uniform mat4 mvpMat;

void main()
{
	ex_Colour = vec4(colour, 1.0f);

	gl_Position = mvpMat * vec4(position, 1.0f);
}