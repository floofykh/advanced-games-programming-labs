// Calculates and passes on V, L, N vectors for use in fragment shader, phong2.frag
#version 330

uniform mat4 modelMatrix;
uniform mat4 modelview;
uniform mat4 projection;
uniform vec4 lightPosition;
//uniform mat3 normalmatrix;
uniform vec3 cameraPos;

layout (location = 0) in vec3 in_Position;
layout (location = 2) in vec3 in_Normal;
out vec3 ex_N;
out vec3 ex_V;
out vec3 ex_L;
out vec3 ex_WorldNorm;
out vec3 ex_WorldView;

layout (location = 3) in vec2 in_TexCoord;
out vec2 ex_TexCoord;

out float ex_distance;

// multiply each vertex position by the MVP matrix
// and find V, L, N vectors for the fragment shader
void main(void) {

	vec3 worldPos = (modelMatrix * vec4(in_Position,1.0)).xyz;
	ex_WorldNorm = mat3(modelMatrix) * in_Normal;
	ex_WorldView = cameraPos - worldPos;

	// vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);

	// Find V - in eye coordinates, eye is at (0,0,0)
	ex_V = normalize(-vertexPosition).xyz;

	// surface normal in eye coordinates
	// taking the rotation part of the modelview matrix to generate the normal matrix
	// (if scaling is includes, should use transpose inverse modelview matrix!)
	// this is somewhat wasteful in compute time and should really be part of the cpu program,
	// giving an additional uniform input
	mat3 normalmatrix = transpose(inverse(mat3(modelview)));
	ex_N = normalize(normalmatrix * in_Normal);

	// L - to light source from vertex
	ex_L = normalize(lightPosition.xyz - vertexPosition.xyz);

	ex_distance = distance(ex_V, ex_L);

	ex_TexCoord = in_TexCoord;

    gl_Position = projection * vertexPosition;

}