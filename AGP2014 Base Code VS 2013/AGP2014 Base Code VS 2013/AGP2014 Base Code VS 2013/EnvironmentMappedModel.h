#pragma once
#include "Model.h"
#include "Cubemap.h"
class EnvironmentMappedModel :
	public Model
{
private:
	Cubemap *environment;
public:
	EnvironmentMappedModel();
	virtual void draw(glm::mat4 viewMat, glm::mat4 projMat);
	void setEnvironment(Cubemap *environment) { this->environment = environment; }
	~EnvironmentMappedModel();
};

