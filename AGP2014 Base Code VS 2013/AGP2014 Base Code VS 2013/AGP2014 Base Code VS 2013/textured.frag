// textured.frag
#version 330

// Some drivers require the following
precision highp float;

uniform sampler2D texMap;

in vec2 ex_TexCoord;
out vec4 out_Color;
 
void main(void) {
    
	// Fragment colour
	out_Color = texture(texMap, ex_TexCoord);
}