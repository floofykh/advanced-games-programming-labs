#pragma once
#include <glm\glm.hpp>
#include <GL\glew.h>
#include "Model.h"

class FrameBufferObject
{
private:
	GLuint bufferId, depthBufferId, reflectionTex, screenWidth, screenHeight, reflectionWidth, reflectionHeight;
public:
	glm::vec3 position = glm::vec3(0.0f), rotation = glm::vec3(0.0f), scale = glm::vec3(1.0f);
	Shader *shader;
	Model *mesh;

	FrameBufferObject(GLuint screenWidth, GLuint screenheight, GLuint reflectionWidth, GLuint reflectionHeight);
	void init();
	glm::mat4 draw(glm::mat4 viewMat, glm::mat4 projMat, unsigned int pass);
	~FrameBufferObject();
};

