// MD2 animation renderer
// This demo will load and render an animated MD2 model, an OBJ model and a skybox
// Most of the OpenGL code for dealing with buffer objects, etc has been moved to a 
// utility library, to make creation and display of mesh objects as simple as possible

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include "rt3dObjLoader.h"
#include "FreeCamera.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
#include "md2model.h"
#include <SDL_ttf.h>
#include "Shader.h"
#include <string>
#include "Texture.h"
#include "Model.h"
#include "Skybox.h"
#include "Cubemap.h"
#include "EnvironmentMappedModel.h"
#include "PBOBuffer.h"
#include "FrameBufferObject.h"
#include "ParticleSystem.h"

using namespace std;
using namespace glm;

#define DEG_TO_RADIAN 0.017453293

// Globals
// Real programs don't use globals :-D

GLuint md2VertCount = 0, hobGoblinMesh;

Shader *phongTexShader, *skyboxShader, *toonShader, *envMapShader, *textureShader, *particleShader;
Texture *hobGobTex, *fabricTex, *metalTex, *smokeTex;
Model *bunny;
EnvironmentMappedModel *cube;
Skybox *skybox;
Cubemap *skyboxCubemap;
PBOBuffer *pboBuffer;
FrameBufferObject *fbo;
ParticleSystem *particles;

GLuint screenWidth = 800, screenHeight = 600;

glm::vec4 lightPos(-10.0f, 10.0f, 10.0f, 1.0f); //light position
Light light = Light(
	vec4(0.3f, 0.3f, 0.3f, 1.0f), // ambient
	vec4(1.0f, 1.0f, 1.0f, 1.0f), // diffuse
	vec4(1.0f, 1.0f, 1.0f, 1.0f), // specular
	lightPos, // position
	vec3(1.0f, 0.0f, 0.0f)
	);

Material material1 = Material(
	vec4(0.2f, 0.4f, 0.2f, 1.0f), // ambient
	vec4(0.5f, 1.0f, 0.5f, 1.0f), // diffuse
	vec4(0.0f, 0.1f, 0.0f, 1.0f), // specular
	2.0f  // shininess
	);
Material material2 = Material(
	vec4(0.4f, 0.4f, 1.0f, 1.0f), // ambient
	vec4(0.8f, 0.8f, 1.0f, 1.0f), // diffuse
	vec4(0.8f, 0.8f, 0.8f, 1.0f), // specular
	1.0f  // shininess
	);
Material metalMaterial = Material(
	vec4(0.4f, 0.4f, 0.4f, 1.0f), // ambient
	vec4(0.4f, 0.4f, 0.4f, 1.0f), // diffuse
	vec4(0.8f, 0.8f, 0.8f, 1.0f), // specular
	2.0f  // shininess
	);
Material bunnyMaterial = Material(
	vec4(0.5f, 0.5f, 5.0f, 1.0f), // ambient
	vec4(0.8f, 0.8f, 0.8f, 1.0f), // diffuse
	vec4(0.8f, 0.8f, 0.8f, 1.0f), // specular
	1.0f  // shininess
	);

// md2 stuff
md2model tmpModel;
int currentAnim = 0;

FreeCamera *camera;

// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		screenWidth, screenHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

void init(void) 
{
	camera = new FreeCamera();

	phongTexShader = new Shader("phong-tex.vert", "phong-tex.frag", "Phong Textured");
	phongTexShader->setLight(&light);

	skyboxShader = new Shader("skyboxCubemap.vert", "skyboxCubemap.frag", "Skybox");

	toonShader = new Shader("toon.vert", "toon.frag", "Toon");
	toonShader->setLight(&light);

	envMapShader = new Shader("phongEnvMap.vert", "phongEnvMap.frag", "Environment Map");
	envMapShader->setLight(&light);

	textureShader = new Shader("textured.vert", "textured.frag", "Textured");

	particleShader = new Shader("particle.vert", "particle.frag", "Particles");

	hobGobTex = new Texture();
	hobGobTex->load("Textures/hobgoblin2.bmp");
	fabricTex = new Texture();
	fabricTex->load("Textures/fabric.bmp");
	metalTex = new Texture();
	metalTex->load("Textures/Metal/studdedmetal.bmp");
	smokeTex = new Texture();
	smokeTex->load("Textures/smoke1.bmp");
	skyboxCubemap = new Cubemap();
	skyboxCubemap->load("Skyboxes/sky", "skyfront.bmp", "skyback.bmp", "skyright.bmp", "skyleft.bmp", "skytop.bmp", "skybottom.bmp");

	cube = new EnvironmentMappedModel();
	cube->load("cube.obj");
	cube->setMaterial(&metalMaterial);
	cube->setTexture(metalTex);
	cube->setEnvironment(skyboxCubemap);
	cube->setShader(envMapShader);
	cube->setScale(vec3(2.0f, 2.0f, 2.0f));
	cube->setPosition(vec3(5.0f, 3.0f, -20.0f));

	bunny = new Model();
	bunny->load("bunny-5000.obj");
	bunny->setMaterial(&bunnyMaterial);
	bunny->setShader(toonShader);
	bunny->setScale(vec3(10.0f, 10.0f, 10.0f));
	bunny->setPosition(vec3(0.0f, 0.5f, -15.0f));
	
	hobGoblinMesh = tmpModel.ReadMD2Model("tris.MD2");
	md2VertCount = tmpModel.getVertDataCount();

	skybox = new Skybox();
	skybox->setCube(cube);
	skybox->load(skyboxCubemap);
	skybox->setShader(skyboxShader);

	/*pboBuffer = new PBOBuffer(screenWidth, screenHeight);
	pboBuffer->init();
	pboBuffer->position = vec3(-5.0f, -3.0f, -10.0f);
	pboBuffer->scale = vec3(3.0f, -3.0f, 0.01f);
	pboBuffer->setShader(textureShader);
	pboBuffer->setMesh(cube);*/

	fbo = new FrameBufferObject(screenWidth, screenHeight, 512, 512);
	fbo->init();
	fbo->mesh = cube;
	fbo->position = vec3(5.0f, 3.0f, -25.0f);
	fbo->rotation = vec3(0.0f);
	fbo->scale = vec3(2.0f, -1.5f, 0.1f);
	fbo->shader = phongTexShader;

	particles = new ParticleSystem(10000, vec3(0.0f, 0.5f, -15.0f));
	particles->setShader(particleShader);
	particles->setTexture(smokeTex);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_POINT_SPRITE);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
}

void update(float deltaTime) {
	camera->update();
	particles->update();

	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	if (keys[SDL_SCANCODE_W]) camera->moveForward(0.1f);
	if (keys[SDL_SCANCODE_S]) camera->moveForward(-0.1f);
	if (keys[SDL_SCANCODE_A]) camera->moveRight(-0.1f);
	if (keys[SDL_SCANCODE_D]) camera->moveRight(0.1f);
	if (keys[SDL_SCANCODE_R]) camera->moveUp(0.1f);
	if (keys[SDL_SCANCODE_F]) camera->moveUp(-0.1f);

	if (keys[SDL_SCANCODE_LEFT]) light.position.x += 1.0f;
	if (keys[SDL_SCANCODE_RIGHT]) light.position.x += 1.0f;
	if (keys[SDL_SCANCODE_UP]) light.position.y += 1.0f;
	if (keys[SDL_SCANCODE_DOWN]) light.position.y -= 1.0f;
	if (keys[SDL_SCANCODE_PAGEUP]) light.position.z += 1.0f;
	if (keys[SDL_SCANCODE_PAGEDOWN]) light.position.z -= 1.0f;

	if (keys[SDL_SCANCODE_COMMA]) camera->rotateY(-1.0f);
	if (keys[SDL_SCANCODE_PERIOD]) camera->rotateY(1.0f);

	if ( keys[SDL_SCANCODE_1] ) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDisable(GL_CULL_FACE);
	}
	if ( keys[SDL_SCANCODE_2] ) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glEnable(GL_CULL_FACE);
	}
	if ( keys[SDL_SCANCODE_Z] ) {
		if (--currentAnim < 0) currentAnim = 19;
		cout << "Current animation: " << currentAnim << endl;
	}
	if ( keys[SDL_SCANCODE_X] ) {
		if (++currentAnim >= 20) currentAnim = 0;
		cout << "Current animation: " << currentAnim << endl;
	}
}


void draw(SDL_Window * window) {
	// clear the screen
	glEnable(GL_CULL_FACE);
	glClearColor(0.1f,0.5f,0.1f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 projection = glm::perspective(60.0f, (float)screenWidth / screenHeight, 1.0f, 150.0f);

	vec3 cameraPos = camera->getPosition();
	envMapShader->setUniformVec3("cameraPos", value_ptr(cameraPos));

	/*for (int i = 0; i < 2; i++)
	{*/
		mat4 viewMat = camera->getViewMat();

		//viewMat = fbo->draw(viewMat, projection, i);
		skybox->draw(viewMat, projection);

		cube->draw(viewMat, projection);
		cube->rotate(vec3(1.0f, 0.0f, 1.0f));

		bunny->draw(viewMat, projection);

		particles->draw(viewMat, projection);

		//pboBuffer->draw(viewMat, projection);
	//}

    SDL_GL_SwapWindow(window); // swap buffers
}


// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		update(1.0f);
		draw(hWindow); // call the draw function
	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}