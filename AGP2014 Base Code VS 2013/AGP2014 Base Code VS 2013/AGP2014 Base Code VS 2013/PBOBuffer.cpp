#include "PBOBuffer.h"
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>

using namespace glm;

PBOBuffer::PBOBuffer(GLuint screenWidth, GLuint screenHeight) : texWidth(screenWidth), texHeight(screenHeight), position(0.0f), scale(1.0f), rotation(0.0f)
{
}

void PBOBuffer::init()
{
	glGenTextures(1, &texIndex);
	GLuint pboTexSize = texWidth * texHeight * 3 * sizeof(GLubyte);
	void *pboData = new GLubyte[pboTexSize];

	//Generate texture
	glBindTexture(GL_TEXTURE_2D, texIndex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texWidth, texHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, pboData); //Initialised a black texture.

	//Generate PBO buffer and send in the texture data. 
	glGenBuffers(1, &bufferIndex);
	glBindBuffer(GL_PIXEL_PACK_BUFFER, bufferIndex);
	glBufferData(GL_PIXEL_PACK_BUFFER, pboTexSize, pboData, GL_DYNAMIC_COPY);
	glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

	glBindTexture(GL_TEXTURE_2D, 0);

	delete pboData;
}

void PBOBuffer::draw(glm::mat4 viewMat, glm::mat4 projMat)
{
	if (shader != nullptr && mesh != nullptr)
	{
		shader->bindProgram();
		shader->setUniformMat4("projection", value_ptr(projMat));

		//Copy back buffer pixels to the PBO.
		glBindTexture(GL_TEXTURE_2D, texIndex);
		glBindBuffer(GL_PIXEL_PACK_BUFFER, bufferIndex);
		//Reads pixels from coord (0, 0) to (texWidth, texHeight) (From bottom right) of the screen, and copies it into the PixelBuffer. 
		glReadPixels(0, 0, texWidth, texHeight, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

		//Copy from PBO to the texture.
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, bufferIndex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texWidth, texHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

		mat4 modelViewMat(1.0f);
		modelViewMat = translate(modelViewMat, position);
		modelViewMat = glm::scale(modelViewMat, scale);
		modelViewMat = glm::rotate(modelViewMat, rotation.x, vec3(1.0f, 0.0f, 0.0f));
		modelViewMat = glm::rotate(modelViewMat, rotation.y, vec3(0.0f, 1.0f, 0.0f));
		modelViewMat = glm::rotate(modelViewMat, rotation.z, vec3(0.0f, 0.0f, 1.0f));

		shader->setUniformMat4("modelview", value_ptr(modelViewMat));

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texIndex);
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		GLuint indexCount, meshID;
		mesh->getIDAndIndexCount(meshID, indexCount);
		glBindVertexArray(meshID);
		glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
	}
}

void PBOBuffer::setDimensions(GLuint screenWidth, GLuint screenHeight)
{
	texWidth = screenWidth;
	texHeight = screenHeight;
}


PBOBuffer::~PBOBuffer()
{
}
