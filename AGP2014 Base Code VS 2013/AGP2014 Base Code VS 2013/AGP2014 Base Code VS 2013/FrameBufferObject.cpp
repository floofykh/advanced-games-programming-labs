#include "FrameBufferObject.h"
#include <iostream>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

using namespace std;
using namespace glm;


static const GLenum fboAttachments[] = { GL_COLOR_ATTACHMENT0 };
static const GLenum defaultFramebuffer[] = { GL_BACK_LEFT };


FrameBufferObject::FrameBufferObject(GLuint screenWidth, GLuint screenheight, GLuint reflectionWidth, GLuint reflectionHeight) :
screenWidth(screenWidth), screenHeight(screenHeight), reflectionWidth(reflectionWidth), reflectionHeight(reflectionHeight)
{
}

void FrameBufferObject::init()
{
	//Generate buffers and texture.
	glGenFramebuffers(1, &bufferId);
	glGenRenderbuffers(1, &depthBufferId);
	glGenTextures(1, &reflectionTex);

	//Bind and init.
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bufferId);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBufferId);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, reflectionWidth, reflectionHeight);
	glBindTexture(GL_TEXTURE_2D, reflectionTex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, reflectionWidth, reflectionHeight, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA, reflectionWidth, reflectionHeight);

	//Attaches the texture created aboce as a Color Attachment to the Framebuffer Object
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, reflectionTex, 0);
	//Attaches the Renderbuffer created above to the Framebuffer as a Depth Attachment. 
	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBufferId);

	//Check for errors.
	GLenum valid = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
	if (valid != GL_FRAMEBUFFER_COMPLETE)
		cout << "Framebuffer Object not complete..." << endl;
	if (valid == GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT)
		cout << "Frambuffer incomplete attachment..." << endl;
	if (valid == GL_FRAMEBUFFER_UNSUPPORTED)
		cout << "FBO attachments unsupported..." << endl;

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

mat4 FrameBufferObject::draw(mat4 viewMat, mat4 projMat, unsigned int pass)
{
	if (pass == 0)
	{
		//Construct virtual world and render to fbo.
		//Bind the framebuffer
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bufferId);
		//Set the scene to draw to the Color Attachment of the Framebuffer (The texture)
		glDrawBuffers(1, fboAttachments);
		//Set the viewport to the size of the mirror. 
		glViewport(0, 0, reflectionWidth, reflectionHeight);
		//Clear the framebuffer
		glClearColor(0.1f, 0.1f, 0.5f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//Do transformations to mirror in the XZ plane. 
		mat4 newViewMat = viewMat;
		newViewMat = translate(newViewMat, position);
		newViewMat = rotate(newViewMat, rotation.y, vec3(0.0f, 1.0f, 0.0f));
		newViewMat = glm::scale(newViewMat, vec3(1.0f, 1.0f, -1.0f));
		newViewMat = rotate(newViewMat, -rotation.y, vec3(0.0f, 1.0f, 0.0f));
		newViewMat = translate(newViewMat, -position);
		glCullFace(GL_FRONT);

		//Return the viewMat to apply to all objects in the scene. 
		return newViewMat;
	}
	else
	{
		//Unbind the created framebuffer
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		//Set the scene to draw to the back framebuffer (the default one).
		glDrawBuffers(1, defaultFramebuffer);
		//Set viewport back to the window size. 
		glViewport(0, 0, screenWidth, screenHeight);
		glClearColor(0.5f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glCullFace(GL_BACK);

		//Draw mirror
		shader->bindProgram();
		shader->setUniformMat4("projection", value_ptr(projMat));
		mat4 modelMat = mat4(1.0f);
		modelMat = translate(modelMat, position);
		modelMat = rotate(modelMat, rotation.y, vec3(0.0f, 1.0f, 0.0f));
		modelMat = glm::scale(modelMat, scale);
		mat4 modelView = viewMat * modelMat;
		shader->setUniformMat4("modelview", value_ptr(modelView));
		shader->setUniform1i("texMap", 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, reflectionTex);
		mesh->drawRawData();
		glBindTexture(GL_TEXTURE_2D, 0);

		return viewMat;
	}
}

FrameBufferObject::~FrameBufferObject()
{
}
