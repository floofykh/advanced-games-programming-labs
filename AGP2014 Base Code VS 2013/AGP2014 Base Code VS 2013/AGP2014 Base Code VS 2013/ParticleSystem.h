#pragma once
#include <GL\glew.h>
#include "Shader.h"
#include <glm\glm.hpp>
#include "Texture.h"
#include <glm\glm.hpp>
class ParticleSystem
{
private:
	GLuint numParticles = 0;
	GLuint vao, vbo[2];
	glm::vec3 position = glm::vec3(0.0f);
	GLfloat *positions = nullptr;
	GLfloat *colours = nullptr;
	GLfloat *velocities = nullptr;


	Texture *texture = nullptr;
	Shader *shader = nullptr;
public:
	ParticleSystem(const unsigned int num, glm::vec3 position);
	unsigned int getNumParticles() { return numParticles; }
	float* getPositions(){ return positions; }
	float* getColours(){ return colours; }
	float* getVelocities(){ return velocities; }
	void setShader(Shader *shader) { this->shader = shader; }
	void setTexture(Texture *texture) { this->texture = texture; }
	void update();
	void draw(glm::mat4 viewMat, glm::mat4 projection);
	~ParticleSystem();
};

