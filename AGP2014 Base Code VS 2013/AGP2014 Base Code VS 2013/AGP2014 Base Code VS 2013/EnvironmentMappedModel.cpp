#include "EnvironmentMappedModel.h"
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

using namespace glm;

EnvironmentMappedModel::EnvironmentMappedModel()
{
}

void EnvironmentMappedModel::draw(glm::mat4 viewMat, glm::mat4 projMat)
{
	if (shader != nullptr)
	{
		shader->bindProgram();
		if (material != nullptr)
			shader->setMaterial(material);
		if (texture != nullptr)
			texture->bind(GL_TEXTURE1);
		if (environment != nullptr)
			environment->bind(GL_TEXTURE0);

		mat4 modelMat(1.0f);
		modelMat = translate(modelMat, position);
		modelMat = glm::scale(modelMat, scale);
		modelMat = glm::rotate(modelMat, rotation.x, vec3(1.0f, 0.0f, 0.0f));
		modelMat = glm::rotate(modelMat, rotation.y, vec3(0.0f, 1.0f, 0.0f));
		modelMat = glm::rotate(modelMat, rotation.z, vec3(0.0f, 0.0f, 1.0f));

		mat4 modelViewMat = viewMat * modelMat;

		shader->setUniformMat4("modelMatrix", value_ptr(modelMat));
		shader->setUniformMat4("modelview", value_ptr(modelViewMat));
		shader->setUniformMat4("projection", value_ptr(projMat));

		glBindVertexArray(id);
		if (indexCount != 0)
		{
			glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
		}
		else
		{
			glDrawArrays(GL_TRIANGLES, 0, vertexCount);
		}
		glBindVertexArray(0);
		Texture::unbind();
	}
}

EnvironmentMappedModel::~EnvironmentMappedModel()
{
}
