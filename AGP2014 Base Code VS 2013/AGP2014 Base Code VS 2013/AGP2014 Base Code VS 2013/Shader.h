#pragma once
#include <string>
#include <GL\glew.h>
#include <glm\glm.hpp>
#include "Light.h"
#include "Material.h"

class Shader
{
private:
	std::string name;
	GLuint vertexID, fragmentID, programID;
	Light *light = nullptr;
	char* loadFile(std::string file, int &fileSize);
	void printError(const GLint id);
public:

	enum ShaderAttributes
	{
		POSITION,
		COLOUR,
		NORMAL,
		TEXTURE,
		INDEX
	};
	enum ShaderTextures
	{
		CUBEMAP,
		TEXTUREMAP
	};

	Shader(std::string vertex, std::string fragment, std::string name);
	void bindProgram();
	void setLight(Light *light) { this->light = light; }
	void setMaterial(Material *material);
	void setUniformMat4(std::string name, GLfloat *data);
	void setUniformVec3(std::string name, GLfloat *data);
	void setUniform1i(std::string name, GLuint value);
	GLuint getProgramID() { return programID; }
	bool compile();
	~Shader();
};

